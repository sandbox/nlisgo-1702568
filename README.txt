Description
-----------

An attempt to integrate the online image map editor found at http://www.maschek.hu/imagemap/imgmap

This for now is just a proof of concept. I have looked at how it has been successfully integrated with wysiwyg editors
but I needed to use it in a way that would not have been easy to do with all of the code bundled in one field.

My specific use case was to use the relate module to like node of a certain content type and allow a guided search of
image maps for you to navigate the assemblies of products.

Before I knew I could integrate it in a way that would meet my specific need I have sought to successfully integrate it
as a standalone module in Drupal.

NOTE: You must download Version 2.2 (build 108) to the libraries folder. Call your folder imgmap_2.2_108 so that
imgmap.js can be found at libraries/imgmap_2.2_108/imgmap.js

Once you install the module go to: admin/config/media/imgmap


Support
-------

Please contact nathan@i-kos.com