<p>
  This example shows how to use the imgmap component in a complete imagemap editor application. This example is
  functionally equivalent to the online editor found at
  <a href="http://www.maschek.hu/imagemap/imgmap">http://www.maschek.hu/imagemap/imgmap</a>.
</p>
<p>
  <?php print l(t('To preview what can be achieved by pre-populating this editor click here'), 'admin/config/media/imgmap/' . libraries_get_path('imgmap_2.2_108') . '/examples/example_files/sample4.jpg'); ?>.
</p>
<p>To preview any image accessible on your website then do the following:</p>
<ul>
  <li>Identify the path of your image beyond the site base url (e.g. <?php print variable_get('file_public_path', conf_path() . '/files'); ?>/myimage.jpg)</li>
  <li>Append that path on to the path of this example edit (e.g. <?php print url(drupal_get_path('module', 'imgmap'), array('absolute' => TRUE)); ?>/<?php print variable_get('file_public_path', conf_path() . '/files'); ?>/myimage.jpg)</li>
</ul>
<form id="img_area_form">
  <fieldset class="source_select">
    <legend>
      <a onclick="toggleFieldset(this.parentNode.parentNode)">Select source</a>
    </legend>
    <div>
      <div class="source_desc">Use a sample image:</div>
      <div class="source_url">
        <select id="source_url3">
          <option value="<?php print libraries_get_path('imgmap_2.2_108', TRUE); ?>/examples/example_files/sample1.jpg" >sample 1</option>
          <option value="<?php print libraries_get_path('imgmap_2.2_108', TRUE); ?>/examples/example_files/sample2.jpg" >sample 2</option>
          <option value="<?php print libraries_get_path('imgmap_2.2_108', TRUE); ?>/examples/example_files/sample3.jpg" >sample 3</option>
          <option value="<?php print libraries_get_path('imgmap_2.2_108', TRUE); ?>/examples/example_files/sample4.jpg" >sample 4</option>
        </select>
      </div>
      <a href="javascript:gui_loadImage(document.getElementById('source_url3').value);" class="source_accept">accept</a><br/>
    </div>
  </fieldset>
  <fieldset>
    <legend>
      <a onclick="toggleFieldset(this.parentNode.parentNode)">Image map areas</a>
    </legend>
    <div style="border-bottom: solid 1px #efefef">
      <div id="button_container">
        <!-- buttons come here -->
        <img src="<?php print url(drupal_get_path('module', 'imgmap')); ?>/images/add.gif" onclick="myimgmap.addNewArea()" alt="Add new area" title="Add new area"/>
        <img src="<?php print url(drupal_get_path('module', 'imgmap')); ?>/images/delete.gif" onclick="myimgmap.removeArea(myimgmap.currentid)" alt="Delete selected area" title="Delete selected area"/>
        <img src="<?php print url(drupal_get_path('module', 'imgmap')); ?>/images/html.gif" onclick="gui_htmlShow()" alt="Get image map HTML" title="Get image map HTML"/>

        <input type="hidden" id="dd_zoom" value="1" />
        <input type="hidden" id="dd_output" value="imagemap" />
      </div>
      <div style="float: right; margin: 0 5px">
        <select onchange="changelabeling(this)">
          <option value=''>No labeling</option>
          <option value='%n' selected='1'>Label with numbers</option>
          <option value='%a'>Label with alt text</option>
          <option value='%h'>Label with href</option>
          <option value='%c'>Label with coords</option>
        </select>
      </div>
    </div>
    <div id="form_container" style="clear: both;">
      <!-- form elements come here -->
    </div>
  </fieldset>
  <fieldset>
    <legend>
      <a onclick="toggleFieldset(this.parentNode.parentNode)">Image</a>
    </legend>
    <div id="pic_container">
    </div>
  </fieldset>
  <fieldset>
    <legend>
      <a onclick="toggleFieldset(this.parentNode.parentNode)">Status</a>
    </legend>
    <div id="status_container"></div>
  </fieldset>
  <fieldset id="fieldset_html" class="fieldset_off">
    <legend>
      <a onclick="toggleFieldset(this.parentNode.parentNode)">Code</a>
    </legend>
    <div>
      <div id="output_help">
      </div>
      <textarea id="html_container"></textarea></div>
  </fieldset>
</form>
<script type="text/javascript" src="<?php print libraries_get_path('imgmap_2.2_108', TRUE); ?>/imgmap.js"></script>
<script type="text/javascript" src="<?php print url(drupal_get_path('module', 'imgmap')); ?>/js/default_interface.js"></script>